#include "devicemanager.h"

deviceManager::deviceManager(QObject *parent) : QObject(parent)
{

}

deviceManager::~deviceManager()
{

}

void deviceManager::createDevices()
{
    remoteControl_vd = new RemoteControl();
    obd2_vd = new OBD2();
    affa3_vd = new AFFA3();
    rdsParser_vd = new RdsParser();
    mainWindow_vd = new MainWindow();
    volumeBar_vd = new volumeBar();
    volumeBar_vd->move(182, 300);
    volumeBar_vd->hide();
}

void deviceManager::initDevices()
{
    obd2_vd->init();
    affa3_vd->init();
    mainWindow_vd->show();
}

void deviceManager::connectDevices()
{
    connect(remoteControl_vd, SIGNAL(pressedButton(int)), affa3_vd, SLOT(sendKeyEvent(int)));
    connect(affa3_vd, SIGNAL(radioTextChanged(can_frame*)), rdsParser_vd, SLOT(parseCanFrame(can_frame*)));
    connect(rdsParser_vd, SIGNAL(sendRadioText(QString)), mainWindow_vd, SLOT(updateRadioText(QString)));
    connect(rdsParser_vd, SIGNAL(sendChangeVolume(int)), volumeBar_vd, SLOT(updateVolume(int)));
    connect(rdsParser_vd, SIGNAL(showVolumeBar(bool)), volumeBar_vd, SLOT(setVisible(bool)));
    connect(obd2_vd, SIGNAL(changeGaugeValue(int,int)), mainWindow_vd, SLOT(updateGaugeValue(int,int)));
    connect(obd2_vd, SIGNAL(changeGaugeValue(int,double)), mainWindow_vd, SLOT(updateGaugeValue(int,double)));
}
