#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include "affa3.h"
#include "remotecontrol.h"
#include "rdsParser.h"
#include "obd2.h"
#include "mainwindow.h"
#include <QObject>

class deviceManager : public QObject
{
    Q_OBJECT
public:
    explicit deviceManager(QObject *parent = 0);
    ~deviceManager();

    void createDevices();
    void initDevices();
    void connectDevices();

private:
    AFFA3 * affa3_vd;
    RemoteControl * remoteControl_vd;
    RdsParser * rdsParser_vd;
    OBD2 * obd2_vd;
    MainWindow * mainWindow_vd;
    volumeBar * volumeBar_vd;

signals:

public slots:
};

#endif // DEVICEMANAGER_H
