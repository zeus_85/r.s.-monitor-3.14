#include "radiowidget.h"

RadioWidget::RadioWidget(QWidget *parent) : GuiWidget(parent), uiRadio(new Ui_RadioWidget())
{
    uiRadio->setupUi(this);
    changeRadioIcons(true, true, true);
}

RadioWidget::~RadioWidget()
{
    delete uiRadio;
}

void RadioWidget::changeRadioText(QString text)
{
    uiRadio->qGuiRadioText->setText(text);
}

void RadioWidget::changeRadioSourceIcon(QPixmap sourceIcon)
{
    uiRadio->qGuiSourceIcon->setPixmap(sourceIcon);
}

void RadioWidget::changeRadioIcons(bool news, bool traffic, bool afrds)
{
    QString icons;

    icons += news ? "NEWS" : "";
    icons += traffic ? " TRAFFIC" : "";
    icons += afrds ? " AF-RDS" : "";

    uiRadio->qRadioIcons->setText(icons);
}

bool RadioWidget::showTaskBarRadioTextAndSourceIcon()
{
    return false;
}
