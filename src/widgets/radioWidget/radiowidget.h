#ifndef RADIOWIDGET_H
#define RADIOWIDGET_H

#include "guiwidget.h"
#include "ui_radiowidget.h"

class RadioWidget : public GuiWidget
{
private:
    Ui_RadioWidget * uiRadio;

public:
    explicit RadioWidget(QWidget * parent = 0);
    ~RadioWidget();

    bool showTaskBarRadioTextAndSourceIcon();

public slots:
    void changeRadioText(QString text);
    void changeRadioSourceIcon(QPixmap sourceIcon);
    void changeRadioIcons(bool news, bool traffic, bool afrds);
};

#endif // RADIOWIDGET_H
