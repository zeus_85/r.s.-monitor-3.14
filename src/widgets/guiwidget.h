#ifndef GUIWIDGET_H
#define GUIWIDGET_H

#include <QWidget>

class GuiWidget : public QWidget
{
public:
    GuiWidget(QWidget *parent = 0) : QWidget(parent){}

    virtual bool showTaskBarRadioTextAndSourceIcon() = 0;

public slots:
    virtual void changeRadioText(QString text) = 0;
    virtual void changeRadioSourceIcon(QPixmap source) = 0;
    virtual void changeRadioIcons(bool news, bool traffic, bool afrds) = 0;
};

#endif // GUIWIDGET_H
