#include "volumebar.h"
#include "ui_volumebar.h"

volumeBar::volumeBar(QFrame *parent) :
    QFrame(parent),
    ui(new Ui::volumeBar)
{
    ui->setupUi(this);
}

volumeBar::~volumeBar()
{
    delete ui;
}

void volumeBar::updateVolume(int volValue)
{
    ui->volumeProgress->setValue(volValue);
}
