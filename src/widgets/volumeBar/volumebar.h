#ifndef VOLUMEBAR_H
#define VOLUMEBAR_H

#include <QFrame>

namespace Ui {
class volumeBar;
}

class volumeBar : public QFrame
{
    Q_OBJECT

public:
    explicit volumeBar(QFrame *parent = 0);
    ~volumeBar();

private:
    Ui::volumeBar *ui;

public slots:
    void updateVolume(int volValue);
};

#endif // VOLUMEBAR_H
