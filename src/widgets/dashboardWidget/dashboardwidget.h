#ifndef DASHBOARDWIDGET_H
#define DASHBOARDWIDGET_H

#include "guiwidget.h"
#include "ui_dashboardwidget.h"

class DashboardWidget : public GuiWidget
{
private:
    Ui_DashboardWidget * uiDashboard;

public:
    explicit DashboardWidget(QWidget *parent = 0);
    ~DashboardWidget();

    bool showTaskBarRadioTextAndSourceIcon();
    void init();

public slots:
    void changeRadioText(QString text);
    void changeRadioSourceIcon(QPixmap sourceIcon);
    void changeRadioIcons(bool news, bool traffic, bool afrds);
    void changeGaugeValue(int gauge, int value);
    void changeGaugeValue(int gauge, double value);
};

#endif // DASHBOARDWIDGET_H
