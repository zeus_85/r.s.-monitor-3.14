#include "dashboardwidget.h"

DashboardWidget::DashboardWidget(QWidget *parent) : GuiWidget(parent), uiDashboard(new Ui_DashboardWidget())
{
    uiDashboard->setupUi(this);
}

DashboardWidget::~DashboardWidget()
{
    delete uiDashboard;
}

bool DashboardWidget::showTaskBarRadioTextAndSourceIcon()
{
    return true;
}

void DashboardWidget::init()
{
    uiDashboard->lcd_top_1->display(0);
    uiDashboard->lcd_top_2->display(0);
    uiDashboard->lcd_top_3->display(0);
    uiDashboard->lcd_bottom_1->display(0);
    uiDashboard->lcd_bottom_2->display(0);
    uiDashboard->lcd_bottom_3->display(0);
    uiDashboard->lcd_bottom_4->display(0);
}

void DashboardWidget::changeRadioText(QString text)
{
    Q_UNUSED(text);
}

void DashboardWidget::changeRadioSourceIcon(QPixmap sourceIcon)
{
    Q_UNUSED(sourceIcon)
}

void DashboardWidget::changeRadioIcons(bool news, bool traffic, bool afrds)
{
    Q_UNUSED(news);
    Q_UNUSED(traffic);
    Q_UNUSED(afrds);
}

void DashboardWidget::changeGaugeValue(int gauge, int value)
{
    switch (gauge) {
    case 11:
        uiDashboard->lcd_top_1->display(value);
        break;
    case 12:
        uiDashboard->lcd_top_2->display(value);
        break;
    case 13:
        uiDashboard->lcd_top_3->display(value);
        break;
    case 21:
        uiDashboard->lcd_bottom_1->display(value);
        break;
    case 22:
        uiDashboard->lcd_bottom_2->display(value);
        break;
    case 23:
        uiDashboard->lcd_bottom_3->display(value);
        break;
    case 24:
        uiDashboard->lcd_bottom_4->display(value);
        break;
    default:
        break;
    }
}

void DashboardWidget::changeGaugeValue(int gauge, double value)
{
    switch (gauge) {
    case 11:
        uiDashboard->lcd_top_1->display(value);
        break;
    case 12:
        uiDashboard->lcd_top_2->display(value);
        break;
    case 13:
        uiDashboard->lcd_top_3->display(value);
        break;
    case 21:
        uiDashboard->lcd_bottom_1->display(value);
        break;
    case 22:
        uiDashboard->lcd_bottom_2->display(value);
        break;
    case 23:
        uiDashboard->lcd_bottom_3->display(value);
        break;
    case 24:
        uiDashboard->lcd_bottom_4->display(value);
        break;
    default:
        break;
    }
}
