#ifndef REMOTECONTROL_H
#define REMOTECONTROL_H

#include <QObject>
#include <QTimer>

enum remoteControlLines{
    RollLine = 25,
    SourceLine = 24,
    VolLoadLine = 23
};

enum remoteControlKeys{
    Load = 0x0000,
    SrcLeft = 0x0001,
    SrcRight = 0x0002,
    VolUp = 0x0003,
    VolDown = 0x0004,
    Pause = 0x0005,
    RollDn = 0x0101,
    RollUp = 0x0141
};

class RemoteControl : public QObject {
    Q_OBJECT

public:
    explicit RemoteControl(QObject *parent = 0);
    ~RemoteControl();

private:
    int buttonState, keyValue, rollState, rollStateOld;
    QTimer * loopTimer;

    void initButtonLoop();
    int checkOneLine(int line);

private slots:
    void checkButtonLoop();

signals:
    void pressedButton(int button);

};

#endif // REMOTECONTROL_H
