#include "remotecontrol.h"
#include <wiringPi.h>
#include <sys/ioctl.h>
#include <QDebug>

RemoteControl::RemoteControl(QObject *parent) : QObject(parent) {

    wiringPiSetup();
    initButtonLoop();

    rollStateOld = -1;

    loopTimer = new QTimer();
    loopTimer->setInterval(200);
    loopTimer->setSingleShot(false);
    connect(loopTimer,SIGNAL(timeout()),this,SLOT(checkButtonLoop()));
    loopTimer->start();
}

RemoteControl::~RemoteControl() {
}

void RemoteControl::initButtonLoop()
{
    pinMode(VolLoadLine, OUTPUT);
    pinMode(SourceLine, OUTPUT);
    pinMode(RollLine, OUTPUT);
    pinMode(27, INPUT);
    pinMode(28, INPUT);
    pinMode(29, INPUT);
}

int RemoteControl::checkOneLine(int line)
{
    int returnState = 0;

    digitalWrite(line, HIGH);
    for(int i=27; i<30; i++)
        if(digitalRead(i))
            returnState += i;
    digitalWrite(line, LOW);

    return returnState;
}

void RemoteControl::checkButtonLoop()
{
    keyValue = -1;

    rollState = checkOneLine(RollLine);
    if(rollState != rollStateOld)
    {
        qDebug() << "rollStateNew = " << QString(rollState);
        qDebug() << "rollStateOld = " << QString(rollStateOld);
        if(rollStateOld >= 0)
        {
            if((rollStateOld+1)%3 == rollState)
                keyValue = RollUp;
            else
                keyValue = RollDn;
        }
        rollStateOld = rollState;
    }

    buttonState = checkOneLine(SourceLine);
    switch(buttonState)
    {
    case 27:
        keyValue = SrcLeft;
        break;
    case 28:
        keyValue = SrcRight;
        break;
    case 29:
        keyValue = Pause;
        break;
    default:
        break;
    }

    buttonState = checkOneLine(VolLoadLine);
    switch (buttonState)
    {
    case 27:
        keyValue = Load;
        break;
    case 28:
        keyValue = VolDown;
        break;
    case 29:
        keyValue = VolUp;
        break;
    case 57:
        keyValue = Pause;
        break;
    default:
        break;
    }

    if(keyValue>=0)
    {
        qDebug() << "Key Value = " << keyValue;
        emit pressedButton(keyValue);
    }
}
