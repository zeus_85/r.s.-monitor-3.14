#include "affa3.h"
#include <QDebug>
#include <QProcess>

bool AFFA3::openCanInterface()
{
    struct sockaddr_can addr;
    struct ifreq ifr;

    if ((canMultimediaSocket = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        qDebug("DisplayEmulator: socket(): fail");
        return false;
    }

    strcpy(ifr.ifr_name, DISPLAY_CAN_INTERFACE);
    ioctl(canMultimediaSocket, SIOCGIFINDEX, &ifr);

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    if (bind(canMultimediaSocket, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        qDebug("AFFA3: Socket bind() failed!");
        close(canMultimediaSocket);
        return false;
    }
    return true;
}

void AFFA3::init()
{
    if(openCanInterface())
    {
        canMultimediaNotifier = new QSocketNotifier(canMultimediaSocket, QSocketNotifier::Read, this);
        connect(canMultimediaNotifier, SIGNAL(activated(int)), this, SLOT(receivedFrame(int)));
        canMultimediaNotifier->setEnabled(true);

        heartbeatState = heartbeatLost;
        heartbeatTimer = new QTimer(this);
        heartbeatTimer->setInterval(1000);
        heartbeatTimer->setSingleShot(true);
        connect(heartbeatTimer, SIGNAL(timeout()), this, SLOT(heartbeatTimeout()));
    }
}

AFFA3::AFFA3(QObject *parent) : QObject(parent)
{

}

AFFA3::~AFFA3()
{
    delete heartbeatTimer;
    delete canMultimediaNotifier;
    close(canMultimediaSocket);
}

void AFFA3::sendCanFrame(can_frame *frame)
{
//    qDebug() << "Sending can_frame ID =" << QString(frame->can_id);
    write(canMultimediaSocket, frame, sizeof(struct can_frame));
}

void AFFA3::sendCanReplay(can_frame *frame, bool last)
{
    int frameIndex;

    frame->can_id |= CAN_ID_REPLY_FLAG;
    frame->can_dlc = 8;

    frameIndex = 0;
    if(last) {
        frame->data[frameIndex++] = 0x74;
    }
    else {
        frame->data[frameIndex++] = 0x30;
        frame->data[frameIndex++] = 0x01;
        frame->data[frameIndex++] = 0x00;
    }

    for( ; frameIndex < 8; frameIndex++)
        frame->data[frameIndex] = DISPLAY_FILL_BYTE;

    sendCanFrame(frame);
}

void AFFA3::sendDisplayStatusFrame(bool isEnabled)
{
    struct can_frame frame;

    frame.can_id = CAN_ID_DISPLAY_STATUS;
    frame.can_dlc = 8;
    frame.data[0] = (isEnabled) ? 0x02 : 0x00;
    frame.data[1] = 0x64;
    frame.data[2] = 0x0F;
    frame.data[3] = DISPLAY_FILL_BYTE;
    frame.data[4] = DISPLAY_FILL_BYTE;
    frame.data[5] = DISPLAY_FILL_BYTE;
    frame.data[6] = DISPLAY_FILL_BYTE;
    frame.data[7] = DISPLAY_FILL_BYTE;

    sendCanFrame(&frame);
}

void AFFA3::sendHeartbeatFrame()
{
    struct can_frame frame;

    frame.can_id = CAN_ID_SYNC_DISPLAY;
    frame.can_dlc = 8;
    frame.data[0] = 0x69;
    frame.data[1] = 0x00;
    frame.data[2] = DISPLAY_FILL_BYTE;
    frame.data[3] = DISPLAY_FILL_BYTE;
    frame.data[4] = DISPLAY_FILL_BYTE;
    frame.data[5] = DISPLAY_FILL_BYTE;
    frame.data[6] = DISPLAY_FILL_BYTE;
    frame.data[7] = DISPLAY_FILL_BYTE;

    sendCanFrame(&frame);
}

void AFFA3::sendHearbeatSyncFrame()
{
    struct can_frame frame;

    frame.can_id = CAN_ID_SYNC_DISPLAY;
    frame.can_dlc = 8;
    frame.data[0] = 0x61;
    frame.data[1] = 0x11;
    frame.data[2] = 0x01;
    frame.data[3] = DISPLAY_FILL_BYTE;
    frame.data[4] = DISPLAY_FILL_BYTE;
    frame.data[5] = DISPLAY_FILL_BYTE;
    frame.data[6] = DISPLAY_FILL_BYTE;
    frame.data[7] = DISPLAY_FILL_BYTE;

    sendCanFrame(&frame);
}

void AFFA3::sendHeartbeat(can_frame * frame)
{
//    switch (heartbeatState) {
//    case heartbeatLost:
        if (frame->data[0] == 0x7A) /* Początek synchronizacji */
        {
            sendHearbeatSyncFrame();
            heartbeatState = heartbeatSyncPending;
        }
//        break;
//    case heartbeatSyncPending:
        if (frame->data[0] == REGISTER_REQUEST_BYTE)
        {
            registerCanId(CAN_ID_DISPLAY_STATUS);
            registerCanId(CAN_ID_KEY_PRESSED);
            heartbeatState = heartbeatNormal;
        }
//        break;
//    case heartbeatNormal:
        if (frame->data[0] == 0x79) /* PING od radia, odsyłamy PONG */
        {
            sendHeartbeatFrame();
            heartbeatTimer->start(); /* Resetujemy timeout */
        }
//        break;
//    default:
//        break;
//    }
}

void AFFA3::sendDisplayStatus(can_frame *frame)
{
    bool isEnabled;

    if (frame->data[0] == REGISTER_REQUEST_BYTE)
        sendCanReplay(frame);

    if (frame->data[0] == 0x04 && frame->data[1] == 0x52)
    {
        isEnabled = (frame->data[2] == 0x02) ? true : false;
        sendDisplayStatusFrame(true);
        emit displayStatusChanged(isEnabled);
    }
}

void AFFA3::triggerTextChange(can_frame *frame)
{
    int frameIndex;
    bool isLast = false;

    if(frame->data[0] == REGISTER_REQUEST_BYTE)
    {
        sendCanReplay(frame);
    }

    else if((frame->data[0] & 0xF0) == 0x00) /* Ustaw ikony */
    {
        /* TODO!!! */
        qDebug() << "DisplayEmulator: TODO: Set icons";
        //qDebug() << frame->data[3];
        //iconsmask = frame->data[3];
        //mode = frame->data[5];
        //emit displayIconsChanged(iconsmask);
        sendCanReplay(frame);
    }

    else if((frame->data[0] & 0xF0) == 0x10) /* Ustaw tekst */
    {
        emit radioTextChanged(frame);
        sendCanReplay(frame, false);
    }

    else if((frame->data[0] & 0xF0) == 0x20) /* Ciąg dalszy tekstu */
    {
        for(frameIndex = 1; frameIndex < 8; frameIndex++)
        {
            if(frame->data[frameIndex] == 0x00)
                isLast = true;
        }
        emit radioTextChanged(frame);
        sendCanReplay(frame, isLast);
    }
}

void AFFA3::registerCanId(int PID)
{
    struct can_frame frame;

    frame.can_id = PID;
    frame.can_dlc = 8;
    frame.data[0] = REGISTER_REQUEST_BYTE;
    frame.data[1] = DISPLAY_FILL_BYTE;
    frame.data[2] = DISPLAY_FILL_BYTE;
    frame.data[3] = DISPLAY_FILL_BYTE;
    frame.data[4] = DISPLAY_FILL_BYTE;
    frame.data[5] = DISPLAY_FILL_BYTE;
    frame.data[6] = DISPLAY_FILL_BYTE;
    frame.data[7] = DISPLAY_FILL_BYTE;

    sendCanFrame(&frame);
}

void AFFA3::receivedFrame(int socketId)
{
    struct can_frame frame;
    int length;

    if (socketId != canMultimediaSocket) { /* Cos jest nie tak */
        return;
    }

    length = read(socketId, &frame, sizeof(struct can_frame));
    if (length <= 0) /* Błąd odczytu */
        return;

    switch(frame.can_id) {
    case CAN_ID_SYNC_HU: sendHeartbeat(&frame); break;
    case CAN_ID_DISPLAY_CONTROL: sendDisplayStatus(&frame); break;
    case CAN_ID_SET_TEXT: triggerTextChange(&frame); break;
    default: break;// sendCanReplay(&frame);
    }
}

void AFFA3::heartbeatTimeout()
{
    qDebug() << "DisplayEmulator: Timeout!";
    heartbeatState = heartbeatLost;
    /* Reset CANa */
    QProcess::execute("ifdown " DISPLAY_CAN_INTERFACE);
    QProcess::startDetached("ifup " DISPLAY_CAN_INTERFACE);
}

void AFFA3::sendKeyEvent(int keycode)
{
    struct can_frame frame;

    frame.can_id = CAN_ID_KEY_PRESSED;
    frame.can_dlc = 8;
    frame.data[0] = 0x03;
    frame.data[1] = 0x89;
    frame.data[2] = (keycode >> 8);
    frame.data[3] = (keycode & 0xFF);
    frame.data[4] = DISPLAY_FILL_BYTE;
    frame.data[5] = DISPLAY_FILL_BYTE;
    frame.data[6] = DISPLAY_FILL_BYTE;
    frame.data[7] = DISPLAY_FILL_BYTE;

    sendCanFrame(&frame);
}
