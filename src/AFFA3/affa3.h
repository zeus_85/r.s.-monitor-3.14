#ifndef AFFA3_H
#define AFFA3_H

#include <QObject>
#include <QTimer>
#include <QSocketNotifier>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#define DISPLAY_CAN_INTERFACE      "can0"

/* CAN IDs */
#define CAN_ID_SYNC_DISPLAY         0x3CF  /* Pakiet synchronizacyjny, wyświetlacz -> HU */
#define CAN_ID_SYNC_HU              0x3DF  /* Pakiet synchronizacyjny, HU -> wyświetlacz */
#define CAN_ID_DISPLAY_CONTROL      0x1B1  /* Konfiguracja wyświetlacza, HU -> wyświetlacz */
#define CAN_ID_DISPLAY_STATUS       0x1C1  /* Zmiana stanu wyświetlacza, wyświetlacz -> HU */
#define CAN_ID_SET_TEXT             0x121  /* Ustawienie tekstu na wyświetlaczu, HU -> wyświetlacz */
#define CAN_ID_KEY_PRESSED          0x0A9  /* Informacja o naciśnięciu klawisza, wyświetlacz -> HU */
#define CAN_ID_REPLY_FLAG           0x400  /* Flaga ustawiana dla odpowiedzi */

/* Bajty wypełniające */
#define DISPLAY_FILL_BYTE           0xA3
#define HU_FILL_BYTE                0x81
#define REGISTER_REQUEST_BYTE       0x70
#define REGISTER_RESPONSE_BYTE      0x74

/* Ikonki na wyświetlaczu */
#define DISPLAY_ICON_NO_NEWS       (1 << 0)
#define DISPLAY_ICON_NEWS_ARROW    (1 << 1)
#define DISPLAY_ICON_NO_TRAFFIC    (1 << 2)
#define DISPLAY_ICON_TRAFFIC_ARROW (1 << 3)
#define DISPLAY_ICON_NO_AFRDS      (1 << 4)
#define DISPLAY_ICON_AFRDS_ARROW   (1 << 5)
#define DISPLAY_ICON_NO_MODE       (1 << 6)
#define DISPLAY_ICON_MODE_NONE     0xFF

enum heartbeatState {
    heartbeatNormal = 0x01,   /* Synchronizacja OK */
    heartbeatSyncPending,     /* Synchronizacja w trakcie negocjacji */
    heartbeatLost             /* Synchronizacja utracona */
};

class AFFA3 : public QObject {
    Q_OBJECT

public:
    explicit AFFA3(QObject *parent = 0);
    ~AFFA3();

    void init();

private:
    int canMultimediaSocket;
    QSocketNotifier * canMultimediaNotifier;
    QTimer * heartbeatTimer;
    enum heartbeatState heartbeatState;

    bool openCanInterface();

    void sendCanFrame(struct can_frame * frame);
    void sendCanReplay(struct can_frame * frame, bool last = true);

    void sendHeartbeat(struct can_frame * frame);
    void sendDisplayStatus(struct can_frame * frame);
    void triggerTextChange(struct can_frame * frame);

    void registerCanId(int PID);

private slots:
    void receivedFrame(int socketId);
    void heartbeatTimeout(void);

    void sendDisplayStatusFrame(bool isEnabled);

    void sendHeartbeatFrame();
    void sendHearbeatSyncFrame();

signals:
    void displayStatusChanged(bool enabled);
    void radioTextChanged(struct can_frame * frame);

public slots:
    void sendKeyEvent(int keycode);
};

#endif // AFFA3_H
