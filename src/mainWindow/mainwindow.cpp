#include <QTime>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowState(Qt::WindowFullScreen);
    setAttribute(Qt::WA_AcceptTouchEvents, true);

    updateTimeTimer = new QTimer();
    updateTimeTimer->setInterval(1000);
    updateTimeTimer->setSingleShot(false);
    connect(updateTimeTimer,SIGNAL(timeout()),this,SLOT(updateTime()));
    connect(ui->radioButton,SIGNAL(clicked(bool)),this,SLOT(mainWidgetRadio()));
    connect(ui->dashboardButton,SIGNAL(clicked(bool)),this,SLOT(mainWidgetDashboard()));
    connect(ui->buttonGroup,SIGNAL(buttonClicked(QAbstractButton*)),this,SLOT(updateButtons(QAbstractButton*)));
    updateTime();
    updateTimeTimer->start();

    guiMainWidget = NULL;
    guiRadioWidget = new RadioWidget();
    guiDashboardWidget = new DashboardWidget();

    ui->mainWidget->addWidget(guiRadioWidget);
    ui->mainWidget->addWidget(guiDashboardWidget);

    guiMainWidget = guiRadioWidget;
    ui->mainWidget->setCurrentWidget(guiMainWidget);
    ui->radioButton->setStyleSheet("background-color: #900000");

    ui->qTaskBarSourceIcon->setScaledContents(true);
    ui->qTaskBarSourceIcon->setVisible(guiMainWidget->showTaskBarRadioTextAndSourceIcon());
    ui->qTaskBarRadioText->setVisible(guiMainWidget->showTaskBarRadioTextAndSourceIcon());

    radioSourceIcon = new QPixmap();
    radioSourceIcon->load(":/resources/icons/source_fm.png");
    updateRadioSourceIcon(*radioSourceIcon);
    qDebug("DEBUG: MainWindow object created");
    qDebug("DEBUG: buttonGroup.size() = %d", ui->buttonGroup->buttons().size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTime()
{
    ui->qTaskBarTime->setText(QTime::currentTime().toString("HH:mm"));

}

void MainWindow::updateRadioText(QString radioText)
{
    if(guiMainWidget->showTaskBarRadioTextAndSourceIcon())
        ui->qTaskBarRadioText->setText(radioText);
    else if(!guiMainWidget->showTaskBarRadioTextAndSourceIcon())
        guiMainWidget->changeRadioText(radioText);
}

void MainWindow::updateRadioSourceIcon(QPixmap radioSourceIcon)
{
    if(guiMainWidget->showTaskBarRadioTextAndSourceIcon())
        ui->qTaskBarSourceIcon->setPixmap(radioSourceIcon);
    else if(!guiMainWidget->showTaskBarRadioTextAndSourceIcon())
        guiMainWidget->changeRadioSourceIcon(radioSourceIcon);
}

void MainWindow::updateRadioIcons(bool news, bool traffic, bool afrds)
{
    if(!guiMainWidget->showTaskBarRadioTextAndSourceIcon())
        guiMainWidget->changeRadioIcons(news, traffic, afrds);
}

void MainWindow::updateGaugeValue(int gauge, int value)
{
    guiDashboardWidget->changeGaugeValue(gauge, value);
}

void MainWindow::updateGaugeValue(int gauge, double value)
{
    guiDashboardWidget->changeGaugeValue(gauge, value);
}

void MainWindow::updateButtons(QAbstractButton *button)
{
    qDebug("DEBUG: Pressed button = %s", qPrintable(button->objectName()));
    int listCount;
    for(listCount = -2; listCount > -2-ui->buttonGroup->buttons().size(); listCount--)
    {
        if(listCount == ui->buttonGroup->id(button))
            ui->buttonGroup->button(listCount)->setStyleSheet("background-color: #900000");
        else if(ui->buttonGroup->button(listCount)->isEnabled())
            ui->buttonGroup->button(listCount)->setStyleSheet("background-color: #000000");
    }
}

void MainWindow::switchMainWidget(GuiWidget *guiCurrentWidget)
{
    ui->mainWidget->setCurrentWidget(guiCurrentWidget);

    ui->qTaskBarSourceIcon->setScaledContents(true);
    ui->qTaskBarSourceIcon->setVisible(guiCurrentWidget->showTaskBarRadioTextAndSourceIcon());
    this->updateRadioSourceIcon(*radioSourceIcon);

    ui->qTaskBarRadioText->setVisible(guiCurrentWidget->showTaskBarRadioTextAndSourceIcon());
}

void MainWindow::mainWidgetRadio()
{
    guiMainWidget = guiRadioWidget;
    switchMainWidget(guiMainWidget);
}

void MainWindow::mainWidgetDashboard()
{
    guiMainWidget = guiDashboardWidget;
    switchMainWidget(guiMainWidget);
}
