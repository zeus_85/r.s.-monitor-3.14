#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "guiwidget.h"
#include "radiowidget.h"
#include "dashboardwidget.h"
#include "volumebar.h"
#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    GuiWidget * guiMainWidget;
    RadioWidget * guiRadioWidget;
    DashboardWidget * guiDashboardWidget;
    volumeBar * guiVolumeBar;

    QTimer * updateTimeTimer;

    QPixmap * radioSourceIcon;

    void switchMainWidget(GuiWidget *guiCurrentWidget);

private slots:
    void updateTime(void);
    void updateRadioText(QString radioText);
    void updateRadioSourceIcon(QPixmap radioSourceIcon);
    void updateRadioIcons(bool news, bool traffic, bool afrds);
    void updateGaugeValue(int gauge, int value);
    void updateGaugeValue(int gauge, double value);
    void updateButtons(QAbstractButton *button);
    void mainWidgetRadio();
    void mainWidgetDashboard();
};

#endif // MAINWINDOW_H
