#ifndef RDSPARSER_H
#define RDSPARSER_H

#include <linux/can.h>
#include <linux/can/raw.h>
#include <QObject>

enum RadioSource
{
    sourceUnknown = 0,
    sourceFM,
    sourceCD,
    sourceCDChanger,
    sourceAUX
};

class RdsParser : public QObject {
    Q_OBJECT
public:
    explicit RdsParser(QObject *parent = 0);
    ~RdsParser();

private:
    QString radioTextBuffer;
    enum RadioSource sourceCurrent;
    bool sourcePaused, isString, isLastFrame;

    void clearBufferParams();
    void extractFrameToText(can_frame *frame);

public slots:
    void parseCanFrame(struct can_frame * frame);

signals:
    void sendChangeVolume(int volumeValue);
    void showVolumeBar(bool show);
    void sendRadioText(QString radioText);
    void sendSourceIcon(enum RadioSource sourceIcon);
};

#endif // RDSPARSER_H
