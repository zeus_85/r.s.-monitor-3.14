#include "rdsParser.h"
#include <QDebug>

RdsParser::RdsParser(QObject *parent) : QObject(parent)
{
    clearBufferParams();
}

RdsParser::~RdsParser()
{

}

void RdsParser::clearBufferParams()
{
    radioTextBuffer.clear();
    isString = false;
    isLastFrame = false;
}

void RdsParser::extractFrameToText(can_frame *frame)
{
    for(int frameIndex = 1; frameIndex < 8; frameIndex++)
    {
        if(isString)
            radioTextBuffer += QChar(frame->data[frameIndex]);
        else if(frame->data[frameIndex] == 0x10)
            isString = true;
        if(frame->data[frameIndex] == 0x00)
        {
            isString = false;
            isLastFrame = true;
            radioTextBuffer.trimmed();
        }
    }
}

void RdsParser::parseCanFrame(can_frame *frame)
{
    if((frame->data[0] & 0xF0) == 0x10)
        clearBufferParams();

    else
        extractFrameToText(frame);

    if(isLastFrame)
    {
        qDebug() << "Text to display: " << radioTextBuffer;
        if(radioTextBuffer.contains("VOLUME"))
        {
            emit sendChangeVolume(radioTextBuffer.split("\\s+", QString::SkipEmptyParts)[1].toInt());
            emit showVolumeBar(true);
        }
        else
        {
            emit sendRadioText(radioTextBuffer);
            emit showVolumeBar(false);
        }
    }
}
