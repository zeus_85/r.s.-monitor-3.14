#include "devicemanager.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("R.S. Monitor 3.14");
    app.setOrganizationName("zeus85");

    QFile styleFile("/home/pi/default.css");
    styleFile.open(QFile::ReadOnly);
    QString style = QLatin1String(styleFile.readAll());
    app.setStyleSheet(style);

    deviceManager devMngr;
    devMngr.createDevices();
    devMngr.initDevices();
    devMngr.connectDevices();

    return app.exec();
}
