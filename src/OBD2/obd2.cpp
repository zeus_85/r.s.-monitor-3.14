#include "obd2.h"

OBD2::OBD2(QObject *parent) : QObject(parent)
{

}

OBD2::~OBD2()
{

}

void OBD2::init()
{
    qDebug("DEBUG: Start OBD2 initialization");
    if(openCanInterface())
    {
        canMultimediaNotifier = new QSocketNotifier(canMultimediaSocket, QSocketNotifier::Read, this);
        connect(canMultimediaNotifier, SIGNAL(activated(int)), this, SLOT(receivedFrame(int)));
        canMultimediaNotifier->setEnabled(true);

        frame01.can_id = 0x7E0;
        frame01.can_dlc = 8;
        frame01.data[0] = 0x02;
        frame01.data[1] = 0x21;
        frame01.data[2] = 0xA0;
        frame01.data[3] = 0x00;
        frame01.data[4] = 0x00;
        frame01.data[5] = 0x00;
        frame01.data[6] = 0x00;
        frame01.data[7] = 0x00;

        frame06.can_id = 0x7E0;
        frame06.can_dlc = 8;
        frame06.data[0] = 0x02;
        frame06.data[1] = 0x21;
        frame06.data[2] = 0xA5;
        frame06.data[3] = 0x00;
        frame06.data[4] = 0x00;
        frame06.data[5] = 0x00;
        frame06.data[6] = 0x00;
        frame06.data[7] = 0x00;

        frameAck.can_id = 0x7E0;
        frameAck.can_dlc = 8;
        frameAck.data[0] = 0x30;
        frameAck.data[1] = 0x00;
        frameAck.data[2] = 0x00;
        frameAck.data[3] = 0x00;
        frameAck.data[4] = 0x00;
        frameAck.data[5] = 0x00;
        frameAck.data[6] = 0x00;
        frameAck.data[7] = 0x00;

        frameCount = 0;

        obd2timer = new QTimer(this);
        obd2timer->setInterval(200);
        obd2timer->setSingleShot(false);
        connect(obd2timer, SIGNAL(timeout()), this, SLOT(getOBD2Values()));
        obd2timer->start();

        startDiagnosticSession();
        qDebug("DEBUG: OBD2 initialization done");
    }
}

bool OBD2::openCanInterface()
{
    struct sockaddr_can addr;
    struct ifreq ifr;

    if ((canMultimediaSocket = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        qDebug("OBD2_CAN: socket(): fail");
        return false;
    }

    strcpy(ifr.ifr_name, OBD2_CAN_INTERFACE);
    ioctl(canMultimediaSocket, SIOCGIFINDEX, &ifr);

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    if (bind(canMultimediaSocket, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        qDebug("OBD2_CAN: Socket bind() failed!");
        close(canMultimediaSocket);
        return false;
    }
    qDebug("DEBUG: openCanInterface for can1 done");
    return true;
}

void OBD2::sendCanFrame(can_frame *frame)
{
    write(canMultimediaSocket, frame, sizeof(struct can_frame));
}

int OBD2::calculateRPM(int rawRPM)
{
    return rawRPM;
}

int OBD2::calculateTorque(int rawTorque)
{
    int calcTorque = (rawTorque * 2 - 100);
    return (calcTorque>0)?calcTorque:0;
}

double OBD2::calculateBoost(int rawBoost)
{
    return ((rawBoost * 57.8125 / 1000 + 103 - 1000) / 1000);
}

double OBD2::calculateAirTemp(int rawAirTemp)
{
    return (rawAirTemp * 0.625 - 40);
}

double OBD2::calculateWaterTemp(int rawWaterTemp)
{
    return (rawWaterTemp * 0.625 - 40);
}

int OBD2::calculateHorsePower(int Torque, int RPM)
{
    int calcHorsePower = ((Torque * RPM) * 1.36 / 9549.3);
    return (calcHorsePower>0)?calcHorsePower:0;
}

double OBD2::calculateBatteryVoltage(int rawBatteryVoltage)
{
    return (rawBatteryVoltage * 0.03215 + 8);
}

void OBD2::startDiagnosticSession()
{
    struct can_frame frame;

    frame.can_id = 0x7E0;
    frame.can_dlc = 8;
    frame.data[0] = 0x02;
    frame.data[1] = 0x10;
    frame.data[2] = 0xC0;
    frame.data[3] = 0x00;
    frame.data[4] = 0x00;
    frame.data[5] = 0x00;
    frame.data[6] = 0x00;
    frame.data[7] = 0x00;

    qDebug("DEBUG: Sending startDiagnosticSession Frame");
    sendCanFrame(&frame);
    qDebug("DEBUG: Frame startDiagnosticSession send");
}

void OBD2::receivedFrame(int socketId)
{
    struct can_frame frame;
    int length;

    if (socketId != canMultimediaSocket)
    { /* Cos jest nie tak */
        qDebug("ERROR: socketId != canMultimediaSocket");
        return;
    }

    length = read(socketId, &frame, sizeof(struct can_frame));
    if (length <= 0) /* Błąd odczytu */
    {
        qDebug("WARNING: length <= 0");
        return;
    }

    if (frame.can_id == 0x7E8)
    {
        if (frame.data[0] == 0x10)
        {
            OBD2PID = (frame.data[2] << 8) + frame.data[3];

            if (OBD2PID == 0x61A0)
            {
                BatteryVoltage = calculateBatteryVoltage(frame.data[4]);
                qDebug("DEBUG: Battery Voltage = %.2f", BatteryVoltage);
                emit changeGaugeValue(24, BatteryVoltage);
                WaterTemp = calculateWaterTemp(frame.data[5]);
                qDebug("DEBUG: Water Temp      = %.2f", WaterTemp);
                emit changeGaugeValue(23, WaterTemp);
                AirTemp = calculateAirTemp(frame.data[6]);
                qDebug("DEBUG: Air Temp        = %.2f", AirTemp);
                emit changeGaugeValue(22, AirTemp);
            }
            if (OBD2PID == 0x61A5)
            {
                Torque = calculateTorque(frame.data[4]);
                qDebug("DEBUG: Torque          = %d", Torque);
                emit changeGaugeValue(11, Torque);
            }

            sendCanFrame(&frameAck);
        }
        else if (frame.data[0] == 0x21)
        {
            if (OBD2PID == 0x61A0)
            {
                switch (frame.data[0])
                {
                case 0x21:
                    RPM = calculateRPM((frame.data[3] << 8) + frame.data[4]);
                    qDebug("DEBUG: Rev Per Minute  = %d", RPM);
                    emit changeGaugeValue(13, RPM);
                    HorsePower = calculateHorsePower(Torque, RPM);
                    qDebug("DEBUG: Horse Power     = %d", HorsePower);
                    emit changeGaugeValue(12, HorsePower);
                    break;
                case 0x22:
                    Boost = calculateBoost((frame.data[3] << 8) + frame.data[4]);
                    qDebug("DEBUG: Boost           = %.2f", Boost);
                    emit changeGaugeValue(21, Boost);
                    break;
                default:
                    break;
                }
            }
//            else if (OBD2PID == 0x61A5)
        }
        else if (frame.data[0] != 0x02)
            startDiagnosticSession();
    }
}

void OBD2::getOBD2Values()
{
    frameCount++;
    frameCount %= 2;

    switch(frameCount)
    {
    case 0:
        sendCanFrame(&frame01);
        break;
    case 1:
        sendCanFrame(&frame06);
        break;
    }
}
