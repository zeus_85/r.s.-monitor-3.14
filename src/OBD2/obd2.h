#ifndef OBD2_H
#define OBD2_H

#include <QObject>
#include <QTimer>
#include <QSocketNotifier>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#define OBD2_CAN_INTERFACE "can1"

class OBD2 : public QObject
{
    Q_OBJECT
public:
    explicit OBD2(QObject *parent = 0);
    ~OBD2();

    void init();

private:
    int canMultimediaSocket;
    QSocketNotifier * canMultimediaNotifier;
    QTimer * obd2timer;
    struct can_frame frame01;
    struct can_frame frame06;
    struct can_frame frameAck;

    int OBD2PID, frameCount;

    int RPM, Torque, HorsePower;
    double Boost, AirTemp, WaterTemp, BatteryVoltage;


    bool openCanInterface();

    void startDiagnosticSession();
    void sendCanFrame(struct can_frame * frame);

    int calculateRPM(int rawRPM);
    int calculateTorque(int rawTorque);
    double calculateBoost(int rawBoost);
    double calculateAirTemp(int rawAirTemp);
    double calculateWaterTemp(int rawWaterTemp);
    int calculateHorsePower(int Torque, int RPM);
    double calculateBatteryVoltage(int rawBatteryVoltage);

signals:
    void changeGaugeValue(int gauge, double value);
    void changeGaugeValue(int gauge, int value);

private slots:
    void receivedFrame(int socketId);
    void getOBD2Values();
};

#endif // OBD2_H
