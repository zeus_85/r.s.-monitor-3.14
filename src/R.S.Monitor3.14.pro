#-------------------------------------------------
#
# Project created by QtCreator 2015-03-23T21:11:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = R.S.Monitor3.14
TEMPLATE = app
LIBS += -L/home/zeus/raspi/sysroot/usr/local/lib \
        -L/home/zeus/raspi/sysroot/usr/lib/arm-linux-gnueabihf \
        -lwiringPi

SOURCES += main.cpp \
        mainWindow/mainwindow.cpp \
        widgets/radioWidget/radiowidget.cpp \
        remoteControl/remotecontrol.cpp \
        rdsParser/rdsParser.cpp \
        AFFA3/affa3.cpp \
        deviceManager/devicemanager.cpp \
        widgets/dashboardWidget/dashboardwidget.cpp \
        OBD2/obd2.cpp \
        widgets/volumeBar/volumebar.cpp

HEADERS  += mainWindow/mainwindow.h \
        widgets/guiwidget.h \
        widgets/radioWidget/radiowidget.h \
        remoteControl/remotecontrol.h \
        rdsParser/rdsParser.h \
        AFFA3/affa3.h \
        deviceManager/devicemanager.h \
        widgets/dashboardWidget/dashboardwidget.h \
        OBD2/obd2.h \
        widgets/volumeBar/volumebar.h

FORMS    += mainWindow/mainwindow.ui \
        widgets/radioWidget/radiowidget.ui \
        widgets/dashboardWidget/dashboardwidget.ui \
        widgets/volumeBar/volumebar.ui

RESOURCES += resources/resources.qrc

OTHER_FILES += resources/themes/default.css

INCLUDEPATH += /home/zeus/sysroot/usr/local/include \
        mainWindow \
        widgets \
        widgets/radioWidget \
        widgets/dashboardWidget \
        widgets/volumeBar \
        remoteControl \
        rdsParser \
        AFFA3 \
        OBD2 \
        deviceManager

target.path = /home/pi
INSTALLS += target
